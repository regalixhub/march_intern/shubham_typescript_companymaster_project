"use strict";
fetch('script/json/prob2.json')
    .then(function (resp) { return resp.json(); })
    .then(function (data) {
    var keysSorted = Object.keys(data).sort(function (a, b) { return data[b] - data[a]; });
    var pba = [];
    var numofreg = [];
    for (var i = 0; i < 10; i += 1) {
        pba.push(keysSorted[i]);
        numofreg.push(data[keysSorted[i]]);
    }
    Highcharts.chart('container2', {
        chart: {
            type: 'column',
        },
        title: {
            text: 'Number of Company Registrations per year',
        },
        xAxis: {
            title: {
                text: 'Year ---->',
            },
            categories: pba,
        },
        yAxis: {
            title: {
                text: 'Number of Company Registrations ---->',
            },
        },
        plotOptions: {
            column: {
                dataLabels: {
                    enabled: true,
                },
            },
        },
        series: [{
                name: 'Number of Company Registrations',
                data: numofreg,
            }],
    });
});

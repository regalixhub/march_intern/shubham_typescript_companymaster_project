"use strict";
fetch('./script/json/prob4.json')
    .then(function (resp) { return resp.json(); })
    .then(function (data) {
    var year = [2010];
    for (var i = 0; year[i] < 2019; i += 1) { // stores related year
        year.push(1 + year[i]);
    }
    var series_array = []; //Array input
    var pba = Object.keys(data); // Array for looping 
    for (var i = 0; i < pba.length; i++) {
        series_array.push({
            name: pba[i],
            data: Object.values(data[pba[i]]),
        });
    }
    Highcharts.chart('container4', {
        chart: {
            type: 'column',
        },
        title: {
            text: 'Stacked-bar-chart(Aggregated number of company registrations of all Principal Business Activities per year)',
        },
        xAxis: {
            title: {
                text: '<---- Year ---->',
            },
            categories: year,
        },
        yAxis: {
            title: {
                text: '<---- Number of Company Registraions ---->',
            },
        },
        plotOptions: {
            series: {
                stacking: 'normal',
            },
        },
        series: series_array,
    });
});

fetch('script/json/prob2.json')
  .then(resp => resp.json())
  .then((data) => {
    const keysSorted = Object.keys(data).sort((a, b) => data[b] - data[a]);
    const pba = [];
    const numofreg = [];
    for (let i = 0; i < 10; i += 1) {
      pba.push(keysSorted[i]);
      numofreg.push(data[keysSorted[i]]);
    }
    Highcharts.chart('container2', {
      chart: {
        type: 'column',
      },
      title: {
        text: 'Number of Company Registrations per year',
      },
      xAxis: {
        title: {
          text: 'Year ---->',
        },
        categories: pba,
      },
      yAxis: {
        title: {
          text: 'Number of Company Registrations ---->',
        },
      },
      plotOptions: {
        column: {
          dataLabels: {
            enabled: true,
          },
        },
      },
      series: [{
        name: 'Number of Company Registrations',
        data: numofreg,
      }],
    });
  });
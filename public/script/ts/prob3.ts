const cs = require('csv-parser')
const fv = require('fs')
const PBARegistrations2015:any = {};
fv.createReadStream('/home/shubham/Desktop/shubham_company_master/public/csv/company_master_data_upto_Mar_2015_Maharashtra.csv','utf-8')
  .pipe(cs())
  .on('data', (data:any) => {

    const date=data.DATE_OF_REGISTRATION.split('-');
    if(date[2]==2015)
    {
      if (!PBARegistrations2015[data.PRINCIPAL_BUSINESS_ACTIVITY]) {
        PBARegistrations2015[data.PRINCIPAL_BUSINESS_ACTIVITY] = 1;
    } else {
        PBARegistrations2015[data.PRINCIPAL_BUSINESS_ACTIVITY] += 1;
          }

    }
  })
  .on('end', () => {
    fv.writeFile('/home/shubham/Desktop/type_basics/public/script/json/prob3.json', JSON.stringify(PBARegistrations2015), () => {});
  })
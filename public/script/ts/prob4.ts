const v = require('csv-parser')
const fst = require('fs')
const PBARegistrationsPerYear:any = {};
fst.createReadStream('/home/shubham/Desktop/type_basics/public/source/company_master_data_upto_Mar_2015_Maharashtra.csv','utf-8')
  .pipe(v())
  .on('data', (data:any) => {
    let y;
    const date=data.DATE_OF_REGISTRATION.split('-');
    if (date[2] >= '2010' && date[2] <= '2018') {
      if (!PBARegistrationsPerYear[data.PRINCIPAL_BUSINESS_ACTIVITY]) {
        PBARegistrationsPerYear[data.PRINCIPAL_BUSINESS_ACTIVITY] = {};
        y = PBARegistrationsPerYear[data.PRINCIPAL_BUSINESS_ACTIVITY];
        if (!y[date[2]]) {
          y[date[2]] = 1;
        }
      } else {
        y = PBARegistrationsPerYear[data.PRINCIPAL_BUSINESS_ACTIVITY];
        if (!y[date[2]]) {
          y[date[2]] = 1;
        } else {
          y[date[2]] += 1;
        }
      }
    }

  
  })
  .on('end', () => {
    fst.writeFile('/home/shubham/Desktop/type_basics/public/script/json/prob4.json', JSON.stringify(PBARegistrationsPerYear), () => {});
  })
const sv:any = require('csv-parser')
const fsv:any = require('fs')
const NumberOfRegistrationsPerYear:any= {};
fsv.createReadStream('/home/shubham/Desktop/shubham_company_master/public/csv/company_master_data_upto_Mar_2015_Maharashtra.csv','utf-8')
  .pipe(sv())
  .on('data', (data:any) => {

    const date=data.DATE_OF_REGISTRATION.split('-');
    if (date[2] >= 1980 && date[2] <= 2018) {
        if (!NumberOfRegistrationsPerYear[date[2]]) {
          NumberOfRegistrationsPerYear[date[2]] = 1;
        } else {
          NumberOfRegistrationsPerYear[date[2]] += 1;
        }
      }
  
  })
  .on('end', () => {
    fsv.writeFile('/home/shubham/Desktop/type_basics/public/script/json/prob2.json', JSON.stringify(NumberOfRegistrationsPerYear), () => {});
  })